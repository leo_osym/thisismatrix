﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ThisIsMatrix
{
    class Program
    {
        static void Main(string[] args)
        {
            double[,] arr1 = { {1,2 },
                                {3,4},
                                {5,6 } };
            double[,] arr2 = { { 3, 6 }, { 2, 8 }, {9, 1 } };
            
            Matrix a = new Matrix(arr1);
            Matrix b = new Matrix(arr2);
            Matrix d = Matrix.Sum(a, b);
            d = a + b;
            d = a - b;
            d = a * b;
            Console.WriteLine(d.ToString());
            Matrix bb = Matrix.Transpose(b);
            
            double[,] arr3 = { { 3, 6, 5 }, { 2, 1, 2 }, { 9, 1, 4 } };
            Matrix x = new Matrix(arr3);
            Console.WriteLine(x.ToString());
            double det = Matrix.Determinant(x);
            Console.WriteLine(Matrix.Determinant(x));
            Console.WriteLine(Matrix.Determinant(a));
        }
    }
}
