﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ThisIsMatrix
{
    /// <summary>
    /// Use tools to work with array as matrix
    /// </summary>
    class Matrix
    {
        public double[,] matrix { get; private set; }
        public Matrix() { }
        public Matrix(double[,] matrix)
        {
            this.matrix = matrix;
        }

        #region ToString and Check
        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < matrix.GetLength(0); i++)
            {
                for (int j = 0; j < matrix.GetLength(1); j++)
                {
                    sb.Append(matrix[i, j] + " ");
                }
                sb.Append("\n");
            }
            return sb.ToString();
        }
        public static bool CheckMatrixSize(Matrix a, Matrix b)
        {
            if (a.matrix.GetLength(0) == b.matrix.GetLength(0) && a.matrix.GetLength(1) == b.matrix.GetLength(1))
            {
                return true;
            }
            return false;
        }
        public static bool CheckMatrixMultiplication(Matrix a, Matrix b)
        {
            if (a.matrix.GetLength(0) == b.matrix.GetLength(1) && a.matrix.GetLength(1) == b.matrix.GetLength(0))
            {
                return true;
            }
            return false;
        }
        #endregion

        #region Transpose
        public static Matrix Transpose(Matrix a)
        {
            Matrix c = new Matrix(new double[a.matrix.GetLength(1), a.matrix.GetLength(0)]);
            for (int i = 0; i < c.matrix.GetLength(0); i++)
            {
                for (int j = 0; j < c.matrix.GetLength(1); j++)
                {
                    c.matrix[i, j] = a.matrix[j, i];
                }
            }
            return c;
        }
        #endregion

        #region Sum
        public static Matrix Sum(Matrix a, Matrix b)
        {
            Matrix c = new Matrix(new double[a.matrix.GetLength(0), a.matrix.GetLength(1)]);
            if (CheckMatrixSize(a, b))
            {
                for (int i = 0; i < c.matrix.GetLength(0); i++)
                {
                    for (int j = 0; j < c.matrix.GetLength(1); j++)
                    {
                        c.matrix[i, j] = a.matrix[i, j] + b.matrix[i, j];
                    }
                }
            }
            return c;
        }
        public static Matrix operator +(Matrix a, Matrix b)
        {
            Matrix c = new Matrix(new double[a.matrix.GetLength(0), a.matrix.GetLength(1)]);
            if (CheckMatrixSize(a, b))
            {
                for (int i = 0; i < c.matrix.GetLength(0); i++)
                {
                    for (int j = 0; j < c.matrix.GetLength(1); j++)
                    {
                        c.matrix[i, j] = a.matrix[i, j] + b.matrix[i, j];
                    }
                }
            }
            return c;
        }
        #endregion

        #region Subtract
        public static Matrix Subtract(Matrix a, Matrix b)
        {
            Matrix c = new Matrix(new double[a.matrix.GetLength(0), a.matrix.GetLength(1)]);
            if (CheckMatrixSize(a, b))
            {
                for (int i = 0; i < c.matrix.GetLength(0); i++)
                {
                    for (int j = 0; j < c.matrix.GetLength(1); j++)
                    {
                        c.matrix[i, j] = a.matrix[i, j] - b.matrix[i, j];
                    }
                }
            }
            return c;
        }
        public static Matrix operator -(Matrix a, Matrix b)
        {
            Matrix c = new Matrix(new double[a.matrix.GetLength(0), a.matrix.GetLength(1)]);
            if (CheckMatrixSize(a, b))
            {
                for (int i = 0; i < c.matrix.GetLength(0); i++)
                {
                    for (int j = 0; j < c.matrix.GetLength(1); j++)
                    {
                        c.matrix[i, j] = a.matrix[i, j] - b.matrix[i, j];
                    }
                }
            }
            return c;
        }
        #endregion

        #region Multiply
        public static Matrix Multiply(Matrix a, Matrix b)
        {
            Matrix c = new Matrix(new double[a.matrix.GetLength(0), b.matrix.GetLength(1)]);
            if (CheckMatrixMultiplication(a, b))
            {
                if (a.matrix.GetLength(1) == b.matrix.GetLength(0))
                {

                    for (int i = 0; i < c.matrix.GetLength(0); i++)
                    {
                        for (int j = 0; j < c.matrix.GetLength(1); j++)
                        {
                            c.matrix[i, j] = 0;
                            for (int z = 0; z < a.matrix.GetLength(1); z++)
                            {
                                c.matrix[i, j] += a.matrix[i, z] * b.matrix[z, j];
                            }
                        }
                    }
                }
            }
            return c;
        }
        public static Matrix Multiply(Matrix a, double b)
        {
            Matrix c = new Matrix(new double[a.matrix.GetLength(0), a.matrix.GetLength(1)]);
            for (int i = 0; i < c.matrix.GetLength(0); i++)
            {
                for (int j = 0; j < c.matrix.GetLength(1); j++)
                {
                    c.matrix[i, j] = a.matrix[i, j] * b;
                }
            }
            return c;
        }
        public static Matrix Multiply(double a, Matrix b)
        {
            Matrix c = new Matrix(new double[b.matrix.GetLength(0), b.matrix.GetLength(1)]);
            for (int i = 0; i < c.matrix.GetLength(0); i++)
            {
                for (int j = 0; j < c.matrix.GetLength(1); j++)
                {
                    c.matrix[i, j] = b.matrix[i, j] * a;
                }
            }
            return c;
        }

        public static Matrix operator *(Matrix a, Matrix b)
        {
            Matrix c = new Matrix(new double[a.matrix.GetLength(0), b.matrix.GetLength(1)]);
            if (CheckMatrixMultiplication(a, b))
            {
                if (a.matrix.GetLength(1) == b.matrix.GetLength(0))
                {

                    for (int i = 0; i < c.matrix.GetLength(0); i++)
                    {
                        for (int j = 0; j < c.matrix.GetLength(1); j++)
                        {
                            c.matrix[i, j] = 0;
                            for (int z = 0; z < a.matrix.GetLength(1); z++)
                            {
                                c.matrix[i, j] += a.matrix[i, z] * b.matrix[z, j];
                            }
                        }
                    }
                }
            }
            return c;
        }
        public static Matrix operator *(Matrix a, double b)
        {
            Matrix c = new Matrix(new double[a.matrix.GetLength(0), a.matrix.GetLength(1)]);
            for (int i = 0; i < c.matrix.GetLength(0); i++)
            {
                for (int j = 0; j < c.matrix.GetLength(1); j++)
                {
                    c.matrix[i, j] = a.matrix[i, j] * b;
                }
            }
            return c;
        }
        public static Matrix operator *(double a, Matrix b)
        {
            Matrix c = new Matrix(new double[b.matrix.GetLength(0), b.matrix.GetLength(1)]);
            for (int i = 0; i < c.matrix.GetLength(0); i++)
            {
                for (int j = 0; j < c.matrix.GetLength(1); j++)
                {
                    c.matrix[i, j] = b.matrix[i, j] * a;
                }
            }
            return c;
        }
        #endregion

        #region Determinant
        private static double Determinant(double[][] arr)
        {
            // i couldn't write my own algorithm, but found working one on Java
            // from here: https://www.sanfoundry.com/java-program-compute-determinant-matrix/
            // i'll use it in my code until i set up my own algo
            double det = 0;
            int N = arr.GetLength(0);
            if (N == 1)
            {
                det = arr[0][0];
            }
            else if (N == 2)
            {
                det = arr[0][0] * arr[1][1] - arr[1][0] * arr[0][1];
            }
            else
            {
                det = 0;
                for (int j1 = 0; j1 < N; j1++)
                {
                    double[][] m = new double[N - 1][];
                    for (int k = 0; k < (N - 1); k++)
                    {
                        m[k] = new double[N - 1];
                    }
                    for (int i = 1; i < N; i++)
                    {
                        int j2 = 0;
                        for (int j = 0; j < N; j++)
                        {
                            if (j == j1)
                                continue;
                            m[i - 1][j2] = arr[i][j];
                            j2++;
                        }
                    }
                    det += Math.Pow(-1.0, 1.0 + j1 + 1.0) * arr[0][j1] * Determinant(m);
                }
            }
            return det;
        }
        /// <summary>
        /// Computes a determinant of square matrix. If matrix is not square, returns 0
        /// </summary>
        /// <param name="a"></param>
        /// <returns>Square matrix to compute determinant </returns>
        public static double Determinant(Matrix a)
        {
            // transform our array into jagged to make it work with previous code
            if (a.matrix.GetLength(0) != a.matrix.GetLength(1)) return 0;
            else
            {
                double[][] arr = new double[a.matrix.GetLength(0)][];
                for (int i = 0; i < a.matrix.GetLength(0); i++)
                {
                    double[] x = new double[a.matrix.GetLength(0)];
                    for (int j = 0; j < a.matrix.GetLength(0); j++)
                    {
                        x[j] = a.matrix[i, j];
                    }
                    arr[i] = x;
                }
                return Determinant(arr);
            }
        }
        #endregion

    }
}
